- 1.7.0 (7 Dec. 2024): Updated for 1.21.2-1.21.4
- 1.6.0 (13 Oct. 2024): port 1.5.0 to 1.21-1.21.1
- 1.5.0 (13 Oct. 2024): port 1.4.0 to 1.20.5-1.20.6
- 1.4.0 (13 Oct. 2024):
  - fixed [#2](<https://gitlab.com/supersaiyansubtlety/custom_piglin_bartering/-/issues/2>):
    entire gold nugget stacks are taken for barter instead of just one
  - made it so piglins won't drop non-gold barterable items in favor of gold items
- 1.3.1 (29 Aug. 2024): Marked as compatible with 1.21.1
- 1.3.0 (17 Jun. 2024):
  - Updated for 1.21
  - Updated translations
- 1.2.16 (5 May 2024):
  - Updated for 1.20.5-1.20.6
  - Moderate internal changes
- 1.2.15 (24 Jan. 2024): Marked as compatible with 1.20.2-1.20.4
- 1.2.14 (24 Jan. 2024):
  - removed `"replace": true` from piglin_bartering_items, fixes #1 (mythicmetals compat)
  - other minor internal changes
- 1.2.13 (23 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.2.12 (3 Apr. 2023): Updated for 1.19.3 and 1.19.4
- 1.2.11 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.2.10 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.2.9 (9 Jul. 2022): No longer bundles piglib, as it used outdated features of REI, which caused warnings on client
and prevented loading on server.
- 1.2.8 (28 Jun. 2022): Updated for 1.19!
- 1.2.7-1 (18 May 2022): Now properly marked as incompatible with 1.18 and 1.18.1
- 1.2.7 (18 May 2022): Updated for 1.18.2
- 1.2.6 (13 Dec. 2021):
  - Updated for 1.18.1.
  - Changed internals to remove some old jank and rely more on piglib.
- 1.2.5 (4 Dec. 2021):
  - Updated for 1.18!
  - Piglib is back to being bundled.
- 1.2.4 (10 Jul. 2021): Marked as compatible with 1.17.1.
- 1.2.3 (20 Jun. 2021):
  - Updated for 1.17.
  - Temporarily removed piglib integration as it's not ready for 1.17 yet.
- 1.2.2 (21 Apr. 2021):Fixes mod name appearing as {mod_name} in Mod Menu. 
- 1.2.1 (18 Jan. 2021): Updated embeded piglib version, fixing server crash.  
- 1.2 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.1-beta (27 Dec. 2020): Now bundles [piglib](https://www.curseforge.com/minecraft/mc-mods/piglib) for better compatibility.
  If you've made a custom datapack, just rename+move your `data/custom_piglin_bartering/tags/items/barter_items.json` to
  `data/piglib/tags/items/piglin_bartering_items.json` and you're set. 
  Also compatible with newer Fabric API versions. 
- 1.0.6 (3 Nov. 2020): Marked as compatible with 1.16.4, replaced one mixin with a Fabric Api event. 
- 1.0.5 (18 Sep. 2020): Marked as compatible with 1.16.3. 
