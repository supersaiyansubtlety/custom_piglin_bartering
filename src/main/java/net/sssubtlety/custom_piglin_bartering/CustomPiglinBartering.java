package net.sssubtlety.custom_piglin_bartering;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CustomPiglinBartering {
    private CustomPiglinBartering() { }

    public static final String NAMESPACE = "custom_piglin_bartering";

    public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
