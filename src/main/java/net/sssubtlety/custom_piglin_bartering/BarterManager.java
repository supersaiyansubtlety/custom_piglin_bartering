package net.sssubtlety.custom_piglin_bartering;

import com.google.common.collect.ImmutableMap;
import net.minecraft.item.Item;
import net.minecraft.loot.LootTable;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.Holder;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Optional;

import static net.sssubtlety.custom_piglin_bartering.CustomPiglinBartering.LOGGER;
import static net.sssubtlety.custom_piglin_bartering.CustomPiglinBartering.NAMESPACE;

public final class BarterManager {
    private BarterManager() { }

    public static final TagKey<Item> PIGLIN_BARTERING_ITEMS =
        TagKey.of(RegistryKeys.ITEM, Identifier.of("piglib", "piglin_bartering_items"));

    private static ImmutableMap<Identifier, LootTable> barters = ImmutableMap.of();

    public static void buildBarters(DynamicRegistryManager manager) {
        final Registry<LootTable> lootTables = manager.getLookupOrThrow(RegistryKeys.LOOT_TABLE);

        barters = Registries.ITEM.getTag(PIGLIN_BARTERING_ITEMS)
            .map(holders -> holders
                .stream()
                .map(Holder::getValue)
                .filter(item -> {
                    // make sure the item is actually in the tag, which may not be true if the tag was
                    // previously defined in a data pack that got disabled
                    //noinspection deprecation; getBuiltInRegistryHolder()
                    return item.getBuiltInRegistryHolder().isIn(PIGLIN_BARTERING_ITEMS);
                })
                .map(Registries.ITEM::getId)
                .flatMap(itemId -> {
                    final Optional<LootTable> optLootTable = lootTables
                        .getOrEmpty(Identifier.of(NAMESPACE, itemId.getNamespace() + "/" + itemId.getPath()));

                    if (optLootTable.isEmpty()) {
                        LOGGER.error("Missing loot table for barter item: {}", itemId);
                    }

                    return optLootTable.map(lootTable -> Map.entry(itemId, lootTable)).stream();
                })
                .collect(ImmutableMap.toImmutableMap(Map.Entry::getKey, Map.Entry::getValue)))
            .orElseGet(() -> {
                LOGGER.info("No \"{}\" tag defined.", PIGLIN_BARTERING_ITEMS.id());

                return ImmutableMap.of();
            });

        if (barters.isEmpty()) {
            LOGGER.info("No barterable items: bartering disabled.");
        }
    }

    public static boolean isBarterable(Item item) {
        return barters.containsKey(Registries.ITEM.getId(item));
    }

    @NotNull
    public static LootTable getBarterLoot(Item item) {
        //noinspection DataFlowIssue; it's non-null if defaultValue is non-null
        return barters.getOrDefault(Registries.ITEM.getId(item), LootTable.EMPTY);
    }
}
