package net.sssubtlety.custom_piglin_bartering.mixin;

import net.minecraft.entity.mob.PiglinBrain;
import net.minecraft.entity.mob.PiglinEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTables;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.ReloadableRegistries;
import net.minecraft.server.world.ServerWorld;

import net.sssubtlety.custom_piglin_bartering.BarterManager;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;

import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PiglinBrain.class)
abstract class PiglinBrainMixin {
	@Unique
	private static final ThreadLocal<Item> barterItem = new ThreadLocal<>();

	/**
	 * @author supersaiyansubtlety
	 * @reason This conceptually replaces acceptsForBarter's behavior. It does its own checks to see if
	 * the item is accepted.
	 * The new method may return different values than the old method and the old method must not run.
	 */
	@Overwrite
	private static boolean acceptsForBarter(ItemStack stack) {
		final Item item = stack.getItem();
		if (BarterManager.isBarterable(item)) {
			barterItem.set(item);

			return true;
		} else {
			return false;
		}
	}

	@Inject(
		method = "consumeOffHandItem",
		at = @At(
			value = "INVOKE",
			shift = At.Shift.AFTER,
			target = "Lnet/minecraft/entity/mob/PiglinBrain;" +
				"getBarteredItem(Lnet/minecraft/entity/mob/PiglinEntity;)Ljava/util/List;"
		)
	)
	private static void unsetBarterItem(ServerWorld world, PiglinEntity piglin, boolean barter, CallbackInfo ci) {
        barterItem.remove();
	}

	/**
	 * @author supersaiyansubtlety
	 * @reason This replaces the hardcoded {@link LootTables#PIGLIN_BARTERING_GAMEPLAY}
	 * loot table with tables mapped from barter items.
	 */
	@Redirect(method = "getBarteredItem", at = @At(
		value = "INVOKE",
		target = "Lnet/minecraft/registry/ReloadableRegistries$Holder;getLootTable" +
			"(Lnet/minecraft/registry/RegistryKey;)Lnet/minecraft/loot/LootTable;"
	))
	@NotNull
	private static LootTable getLootTableForBarteredItem(
		ReloadableRegistries.Holder instance,
		RegistryKey<LootTable> registryKey,
		PiglinEntity piglin
	) {
		return BarterManager.getBarterLoot(barterItem.get());
	}

	@WrapOperation(
		method = "loot",
		at = @At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;isOf(Lnet/minecraft/item/Item;)Z")
	)
	private static boolean preventDiscardingBarterableStacks(
		ItemStack instance, Item item, Operation<Boolean> original
	) {
		return !BarterManager.isBarterable(instance.getItem()) && original.call(instance, item);
	}

	@ModifyExpressionValue(
		method = "canGather",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/mob/PiglinBrain;doesNotHaveGoldInOffHand" +
				"(Lnet/minecraft/entity/mob/PiglinEntity;)Z"
		)
	)
	private static boolean doesNotHaveBarterableItemInOffhand(boolean original, PiglinEntity piglin) {
		return original && !BarterManager.isBarterable(piglin.getOffHandStack().getItem());
	}
}
