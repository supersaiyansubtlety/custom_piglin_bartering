package net.sssubtlety.custom_piglin_bartering;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.CommonLifecycleEvents;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.util.Identifier;

import static net.fabricmc.fabric.api.resource.ResourceManagerHelper.registerBuiltinResourcePack;
import static net.sssubtlety.custom_piglin_bartering.CustomPiglinBartering.NAMESPACE;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        FabricLoader.getInstance().getModContainer(NAMESPACE).ifPresent(thisContainer -> {
            registerBuiltinResourcePack(
                Identifier.of(NAMESPACE, "default_custom_piglin_barters"),
                thisContainer,
                ResourcePackActivationType.DEFAULT_ENABLED
            );

            registerBuiltinResourcePack(
                Identifier.of(NAMESPACE, "vanilla_piglin_barters"),
                thisContainer,
                ResourcePackActivationType.NORMAL
            );
        });

        CommonLifecycleEvents.TAGS_LOADED.register((registries, client) -> {
            if (!client) {
                BarterManager.buildBarters(registries);
            }
        });
    }
}
