<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Custom Piglin Bartering

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_383529_all.svg)](https://modrinth.com/mod/custom-piglin-bartering/versions#all-versions)
![environment: server](https://img.shields.io/badge/environment-server-orangered)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/custom_piglin_bartering/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/custom_piglin_bartering)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/custom_piglin_bartering?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/custom_piglin_bartering/-/issues)
[![localized: Percentage](https://badges.crowdin.net/custom-piglin-bartering/localized.svg)](https://crwd.in/custom-piglin-bartering)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/custom-piglin-bartering?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/custom-piglin-bartering/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/383529?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/custom-piglin-bartering/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Give piglins different items to receive different loot!

Works server-side and in single player.

Allows data packs to:
1. ~~define a list of items that piglins are attracted to~~ removed because this is now handled by the base game's
`piglin_loved` item tag
2. define a list of items that piglins will barter for
3. map each barterable item to a loot table that represents possible bartering returns

By default, it separates the vanilla barters into 3 tiers mapped to gold nuggets, ingots, and blocks.

Information on creating data packs and other details can be found on
[the wiki](https://gitlab.com/supersaiyansubtlety/custom_piglin_bartering/-/wikis/home).

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/custom-piglin-bartering/localized.svg)](https://crwd.in/custom-piglin-bartering)  
If you'd like to help translate Custom Piglin Bartering, you can do so on
[Crowdin](https://crwd.in/custom-piglin-bartering).  

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however,
so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
